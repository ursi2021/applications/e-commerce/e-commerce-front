import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import vuetify from './plugins/vuetify';
import Cart from './components/Cart.vue';
import Market from './components/Market';
import Order from './components/OrderList';
import Admin from './components/Admin';
import User from './components/User';

Vue.use(VueRouter)
Vue.config.productionTip = false

const router = new VueRouter ({
  routes : [
    { path: '/', component: Market},
    { path: '/cart', component: Cart },
    { path: '/order', component: Order},
    { path: '/admin', component: Admin},
    { path: '/user', component: User},
  ],
  mode: 'history'
});

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
